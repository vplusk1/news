<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = [
        'slug', 'title', 'body', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->withDefault();
    }

    public function comments()
    {
        return $this->hasMany('App\Comments', 'article_id', 'id')->withDefault();
    }
}
