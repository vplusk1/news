<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\ArticleRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\Users\UserProfileRequest;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct(UserRepository $users, ArticleRepository $articles)
    {
        $this->users = $users;
        $this->articles = $articles;
    }

    /**
     * Show all users
     *
     * @return $this
     */
    public function index()
    {
        $users = $this->users->getAll();
        return view('users.index')->with('users', $users);
    }

    /**
     * Show the user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSingleUser($id)
    {
        $user = $this->users->getOneUser($id);
        $articles = $this->articles->getAllByUser($id);

        return view('users.single')->with([
            'user' => $user,
            'articles' => $articles
        ]);
    }

    /**
     * @param UserProfileRequest $request
     *
     * Show user profile
     *
     * @return $this
     */
    public function profile(UserProfileRequest $request)
    {
        $user_id = Auth::user()->id;
        $user = $this->users->getOneUser($user_id);
        $articles = $this->articles->getAllByUser($user_id);

        return view('users.profile')->with([
            'user' => $user,
            'articles' => $articles
        ]);
    }

    /**
     * Subscribe for other users' updates
     *
     * @param Request $request
     * @return mixed
     */
    public function subscribe(Request $request)
    {
        $this->users->subscribe($request->all());

        return redirect()
            ->route('users.index')
            ->withFlashSuccess('You have been subscribed');
    }

}