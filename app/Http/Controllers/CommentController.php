<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CommentsRepository;
use App\Http\Requests\Comments\SaveCommentRequest;

class CommentController extends Controller
{
    public $comments;

    public function __construct(CommentsRepository $comments)
    {
        $this->middleware('auth', ['only' => ['create', 'store']]);
        $this->comments = $comments;
    }

    public function store(SaveCommentRequest $request)
    {
        $comment = $this->comments->store($request);

        return redirect()
            ->back()
            ->withFlashSuccess("Comment has been posted");
    }


}