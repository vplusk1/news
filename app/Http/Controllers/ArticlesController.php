<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ArticleRepository;
use App\Repositories\CommentsRepository;
use App\Http\Requests\Articles\SaveArticleRequest;
use App\Http\Requests\Articles\EditArticleRequest;
use App\Http\Requests\Articles\DeleteArticleRequest;

/**
 * Class ArticlesController
 *
 * @package App\Http\Controllers
 */
class ArticlesController extends Controller
{
    /**
     * @var ArticleRepository
     */
    public $articles;

    /**
     * Create a new controller instance.
     *
     * @param ArticleRepository $articles
     */
    public function __construct(ArticleRepository $articles, CommentsRepository $comments)
    {
        $this->middleware('auth', ['only' => ['create', 'store', 'edit', 'update', 'delete']]);
        $this->articles = $articles;
        $this->comments = $comments;
    }

    /**
     * Show all articles
     *
     * @return $this
     */
    public function index()
    {
        $articles = $this->articles->getAll();
        return view('articles.index')->with('articles', $articles);
    }

    /**
     * @param $slug
     *
     * Show one article
     *
     * @return $this
     */
    public function show($slug)
    {
        $article = $this->articles->getOneArticle($slug);
        $comments = $this->comments->getForArticle($slug);

        return view('articles.single')->with([
            'article' => $article,
            'comments' => $comments
        ]);
    }

    /**
     * Show form for article create
     *
     * @return $this
     */
    public function create()
    {
        return view('articles.create')->with([
            'article'  => $this->articles->create()
        ]);
    }

    /**
     * @param SaveArticleRequest $request
     *
     * @return mixed
     */
    public function store(SaveArticleRequest $request)
    {
        $article = $this->articles->store($request);
        return redirect()
            ->route('articles.show', $article->slug)
            ->withFlashSuccess("Article has been saved");
    }

    /**
     * @param EditArticleRequest $request
     * @param $slug
     *
     * Show edit form for article
     *
     * @return $this
     */
    public function edit(EditArticleRequest $request, $slug)
    {
        return view('articles.edit')->with([
           'article'  => $this->articles->getOneArticle($slug)
        ]);
    }

    /**
     * @param EditArticleRequest $request
     * @param $id
     *
     * Updating article record in database
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditArticleRequest $request, $id)
    {
        $this->articles->update($request, $id);

        return redirect()->route('users.profile');
    }

    /**
     * @param DeleteArticleRequest $request
     * @param $slug
     *
     * Delete article from database
     *
     * @return mixed
     */
    public function destroy(DeleteArticleRequest $request, $slug)
    {
        $this->articles->delete($slug);
        return redirect()
            ->route('users.profile')
            ->withFlashSuccess("Article has been deleted");
    }

}