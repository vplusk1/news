<?php

namespace App\Http\Requests\Articles;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Illuminate\Auth\AuthenticationException;

/**
 * Class SaveArticleRequest
 *
 * @package App\Http\Requests\Article
 */
class SaveArticleRequest extends Request
{

    /**
     * Determine if current user has an access
     *
     * @return bool
     *
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:120', Rule::unique('articles')],
            'body'  => 'required|min:120',
        ];
    }
}