<?php

namespace App\Http\Requests\Articles;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Illuminate\Auth\AuthenticationException;

/**
 * Class EditArticleRequest
 *
 * @package App\Http\Requests\Article
 */
class EditArticleRequest extends Request
{

    /**
     * Determine if current user has an article with given slug
     *
     * @return bool
     * @throws AuthenticationException
     */
    public function authorize()
    {
        $user = app('auth')->user();
        $slug = app('request')->route('article');

        if ($user->articles->where('slug', $slug)->isEmpty())
            throw new AuthenticationException;

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}