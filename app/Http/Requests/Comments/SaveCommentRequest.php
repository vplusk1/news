<?php

namespace App\Http\Requests\Comments;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Illuminate\Auth\AuthenticationException;

/**
 * Class SaveCommentRequest
 *
 * @package App\Http\Requests\Article
 */
class SaveCommentRequest extends Request
{

    /**
     * Determine if current user has an access
     *
     * @return bool
     *
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body'  => 'required|min:10',
        ];
    }
}