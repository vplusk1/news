<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest
 *
 * @package App\Http\Requests\Auth
 */
class RegisterRequest extends Request
{

    /**
     * Determine if current user has an access
     *
     * @return bool
     *
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required|string|max:191',
            'email'          => ['required', 'string', 'email', 'max:191', Rule::unique('users')],
            'password'       => 'required|string|min:6|confirmed',
        ];
    }
}