<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Request;
use Illuminate\Auth\AuthenticationException;


/**
 * Class UserProfileRequest
 *
 * @package App\Http\Requests\Article
 */
class UserProfileRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     * @throws AuthenticationException
     */
    public function authorize()
    {
        $user = app('auth')->user();
        if (!$user) throw new AuthenticationException;

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}