<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'confirmation_code', 'confirmed'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getName()
    {
        return $this->name;
    }

    public function articles()
    {
        return $this->hasMany('App\Article', 'user_id');
    }

    /**
     * Check if user has confirmed his account with email
     *
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed == 1;
    }

    public function isSubscribed($author_id, $subscriber_id)
    {
        $res = Subscribe::where([
            ['author_id', '=', $author_id],
            ['subscriber_id', '=', $subscriber_id]
        ])->first();

        return (empty($res)) ? false : true;
    }
}
