<?php

namespace App\Repositories;

use App\Article;
use App\Notifications\Users\UserHasPostedNewArticle;
use App\Subscribe;
use App\User;

/**
 * Class ArticleRepository
 *
 * @package App\Repositories
 */
class ArticleRepository
{

    /**
     * @return model
     */
    public function model()
    {
        return Article::class;
    }

    /**
     * Get all articles
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getAll()
    {
        return Article::all();
    }

    /**
     * @param $slug
     *
     * Get one article
     *
     * @return mixed
     */
    public static function getOneArticle($slug)
    {
        return Article::where('slug', $slug)->firstOrFail();
    }

    /**
     * @param $id
     *
     * Get all articles, created by user
     *
     * @return mixed
     */
    public function getAllByUser($id)
    {
        return Article::where('user_id', $id)->get();
    }

    /**
     * Create new Article instance
     *
     * @return Article
     */
    public function create()
    {
        return new Article;
    }

    /**
     * @param $input
     *
     * Store new article
     *
     * @return mixed
     */
    public function store($input)
    {
        return $this->save(new Article, $input);
    }

    /**
     * @param $input
     * @param $slug
     *
     * Edit article
     *
     * @return mixed
     */
    public function update($input, $slug)
    {
        return $this->save($this->getOneArticle($slug), $input);
    }

    /**
     * @param $article
     * @param $input
     *
     * Save article to database
     *
     * @return mixed
     */
    protected function save($article, $input)
    {
        $input = $input->all();

        $article->user_id   = app('auth')->user()->id;

        $article->slug  = str_slug($input['title']);
        $article->title = $input['title'];
        $article->body  = $input['body'];

        $article->save();

        $users = Subscribe::where('author_id', app('auth')->user()->id)->pluck('subscriber_id');

        foreach ($users as $user) {
            $user = User::where('id', $user)->first();
            $user->notify(new UserHasPostedNewArticle());
        }

        return $article;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function delete($slug)
    {
        return Article::where('slug', $slug)->firstOrFail()->delete();

    }

}
