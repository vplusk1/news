<?php

namespace App\Repositories;

use App\Comment;
use App\Article;

/**
 * Class ArticleRepository
 *
 * @package App\Repositories
 */
class CommentsRepository
{

    /**
     * @return model
     */
    public function model()
    {
        return Comment::class;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getForArticle($slug)
    {
        $article_id = Article::where('slug', $slug)->firstOrFail()->id;
        $comments = Comment::where('article_id', $article_id)->get();

        return $comments;
    }

    /**
     * @param $input
     * @return mixed
     */
    public function store($input)
    {
        return $this->save(new Comment, $input);
    }

    /**
     * @param $comment
     * @param $input
     * @return mixed
     */
    protected function save($comment, $input)
    {
        $input = $input->all();

        $comment->user_id   = app('auth')->user()->id;
        $comment->body  = $input['body'];
        isset($input['parent_id']) ? $comment->parent_id = $input['parent_id'] : null;
        $comment->article_id = $input['article_id'];

        $comment->save();
        return $comment;
    }

}
