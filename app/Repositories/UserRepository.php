<?php

namespace App\Repositories;

use App\User;
use App\Subscribe;
use App\Notifications\Users\UserNeedsConfirmation;
use App\Exceptions\GeneralException;

/**
 * Class UserRepository
 *
 * @package App\Repositories
 */
class UserRepository
{
    /**
     * @var
     */
    protected $model;

    /**
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get all registered users
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getAll()
    {
        return User::all();
    }

    /**
     * Get one user
     *
     * @param $id
     * @return mixed
     */
    public static function getOneUser($id)
    {
        return User::where('id', $id)->firstOrFail();
    }

    /**
     *
     * @param array $data
     * @param bool $provider
     *
     * @return string
     */
    public function create(array $data, $provider = false)
    {
        $user = $this->model();
        $user = new $user;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->password = $provider ? null : bcrypt($data['password']);
        $confirm = false;

        $user->confirmed = 0;

        $user->save();

        /*
         * If users have to confirm their email and this is not a social account,
         * and the account does not require admin approval
         * send the confirmation email
         *
         */
        $user->notify(new UserNeedsConfirmation($user->confirmation_code));

        /*
         * Return the user object
         */
        return $user;
    }

    /**
     * @param $input
     * @return Subscribe
     */
    public function subscribe($input)
    {
        $author_id = $input['author_id'];
        $subscriber_id = $input['subscriber_id'];

        $subscription = new Subscribe;
        $subscription->author_id = $author_id;
        $subscription->subscriber_id = $subscriber_id;

        $subscription->save();

        return $subscription;

    }

    /**
     * @param $token
     * @return mixed
     */
    public function findByConfirmationToken($token)
    {
        return User::where('confirmation_code', $token)->first();
    }

    /**
     * @param Model $user
     */
    public function confirm($token)
    {
        $user = $this->findByConfirmationToken($token);

        if ($user->confirmed == 1) {
            throw new GeneralException('This user is already confirmed.');
        }

        $user->confirmed = 1;
        $confirmed = $user->save();

    }

}
