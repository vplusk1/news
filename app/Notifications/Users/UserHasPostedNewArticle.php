<?php

namespace App\Notifications\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserHasPostedNewArticle
 * @package App\Notifications\Users
 */
class UserHasPostedNewArticle extends Notification
{

    /**
     * @param $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('The Sunday Times: New post!')
            ->line('User that you are following has posted a new article')
            ->line('Visit our site to see it first!')
            ->action('Confirm Account', route('welcome'));
    }
}