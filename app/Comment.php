<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = [
        'body', 'user_id', 'parent_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->withDefault();
    }

    public function article()
    {
        return $this->belongsTo('App\Article', 'article_id')->withDefault();
    }

    public function getChildren($id)
    {
        return $this->where('parent_id', $id)->get();
    }

}
