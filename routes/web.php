<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

//Route::get('/articles', 'ArticlesController@index')->name('articles.index');
//Route::get('/articles/{slug}', 'ArticlesController@single')->name('articles.single');
//Route::get('/articles/create', 'ArticlesController@create')->name('articles.create');

Route::resource('articles', 'ArticlesController');

Route::post('/comments', 'CommentController@store')->name('comments.store');


Route::get('/users', 'UsersController@index')->name('users.index');
Route::get('/users/{id}', 'UsersController@getSingleUser')->name('users.single');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'UsersController@profile')->name('users.profile');

Route::post('/subscribe', 'UsersController@subscribe')->name('subscribe');