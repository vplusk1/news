# "The Sunday Times": news   

## Installation

1. Clone the repo
2. change directory: `cd news`
3. Install Laravel: `composer install`
4. Change database settings in `env`
5. Perform migration: `php artisan migrate`
6. View application in browser. Register, confirm account with e-mail, post news and discuss it 