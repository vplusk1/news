'use strict';

require('./bootstrap');

import jquery from 'jquery';

window.jQuery = jquery;
window.$ = jquery;

$('.show-reply').click(function (event) {
    event.preventDefault();
    //console.log($(this).closest('.reply-form'));
    $(this).next().css('display', 'block');
});

$('#subscribe_btn').click(function () {
    var query = {
        author_id: $(this).attr('data-author'),
        subscriber_id: $(this).attr('data-subscriber')
    };
    var token = document.head.querySelector('meta[name="csrf-token"]').content;
    var route = $(this).attr('data-route');

    // $.post(route, query, function (resp) {
    //     if(resp && resp.success) {
    //         console.log(resp)
    //     } else {
    //         alert('Something went wrong');
    //     }
    // });
    $.ajax({
        headers: {'X-CSRF-TOKEN': token },
        data: query,
        type: "POST",
        url: route,
        success: function (resp) {
            console.log(resp);
        },
        error: function (err) {
            console.log(err);
        }
    });
});