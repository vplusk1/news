<h4>Comments</h4>

{{ Form::open(['route' => 'comments.store', 'id' => 'comment-form',  "enctype" => "multipart/form-data"]) }}
<div>
    <br>
    {{ Form::textarea('body') }}
    {{ Form::hidden('article_id', $article->id) }}
</div>
<div>
    {{ Form::button("Post", ["class" => "btn btn-primary", "type" => "submit"]) }}
    {{ Form::close() }}
</div>

{{ Form::close() }}

<br><br>
@foreach($comments as $comment)
    @if(empty($comment->parent_id))
        <div>
            {!! $comment->user->name !!} <br>
            {!! date('d-m-Y', strtotime($comment->created_at)) !!} <br>
            {!! $comment->body !!} <br>
            <a href="" class="show-reply">Reply</a>
            <div class="reply-form" style="display: none;">
                {{ Form::open(['route' => 'comments.store', 'id' => 'comment-form',  "enctype" => "multipart/form-data"]) }}
                <div>
                    <br>
                    {{ Form::textarea('body') }}
                    {{ Form::hidden('parent_id', $comment->id) }}
                    {{ Form::hidden('article_id', $article->id) }}
                </div>
                <div>
                    {{ Form::button("Post", ["class" => "btn btn-primary", "type" => "submit"]) }}
                    {{ Form::close() }}
                </div>

                {{ Form::close() }}
            </div>
        </div>
        <hr>
    @endif
        @php($children_comments = $comment->getChildren($comment->id))
        @if(count($children_comments) > 0)
            @foreach($children_comments as $comment)
                <div style="margin-left: 50px;">
                    {!! $comment->user->name !!} <br>
                    {!! date('d-m-Y', strtotime($comment->created_at)) !!} <br>
                    {!! $comment->body !!} <br>
                </div>
                <hr>
            @endforeach
        @endif
@endforeach