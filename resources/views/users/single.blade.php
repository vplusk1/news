@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Users</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            <div>
                                <h3>{!! $user->name !!}</h3>
                                <a href="mailto:{!! $user->email !!}">{!! $user->email !!}</a>
                                <p>
                                    Registration date: {!! date('d-m-Y', strtotime($user->created_at))  !!}
                                </p>
                                @if(app('auth')->user() !== null && app('auth')->user()->id !== $user->id)
                                    @if(app('auth')->user()->isSubscribed($user->id, app('auth')->user()->id))
                                        You have been subscribed
                                    @else
                                        <button data-route="{{ route('subscribe') }}" data-author="{{ $user->id }}" data-subscriber="{{ app('auth')->user()->id }}" id="subscribe_btn" class="btn btn-submit">
                                            Subscribe
                                        </button>
                                    @endif
                                @endif
                            </div>

                            <hr>

                            <div>
                                <h3>Articles by {!! $user->name !!}</h3>
                                <h5>Total count: {!! count($user->articles) !!}</h5>
                                <br><br>
                                @foreach($articles as $article)
                                    <div>
                                        <a href="/articles/{!! $article->slug !!}">{!! $article->title !!}</a>
                                        <p>
                                            {!! $article->body !!}
                                        </p>
                                        <p>{!! date('d-m-Y', strtotime($article->created_at)) !!}</p>
                                    </div>
                                @endforeach
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
