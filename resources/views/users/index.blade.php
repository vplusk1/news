@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Users</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            @foreach($users as $user)
                                <div>
                                    <h4>{!! $user->name !!}</h4>
                                    <a href="/users/{!! $user->id !!}">Read more...</a>
                                </div>

                                <br><br>
                            @endforeach



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
