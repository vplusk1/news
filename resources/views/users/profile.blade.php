@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{!! $user->name !!} profile</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div>
                            <h3>{!! $user->name !!}</h3>
                            <a href="mailto:{!! $user->email !!}">{!! $user->email !!}</a>
                            <p>
                                Registration date: {!! date('d-m-Y', strtotime($user->created_at))  !!}
                            </p>
                        </div>

                        <hr>

                        <div>
                            <h3>Articles by {!! $user->name !!}</h3>
                            <h5>Total count: {!! count($user->articles) !!}</h5>
                            <br>
                            <a href="{{ route('articles.create') }}" title="Добавить"><span>Create article</span></a>
                            <br><br>
                            @foreach($articles as $article)
                                <div>
                                    <a href="/articles/{!! $article->slug !!}">{!! $article->title !!}</a>
                                    / <a href="{{ route('articles.edit', $article->slug) }}">Edit</a>
                                    <p>
                                        {!! $article->body !!}
                                    </p>
                                    <p>{!! date('d-m-Y', strtotime($article->created_at)) !!}</p>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
