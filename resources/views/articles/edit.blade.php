@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Articles</div>

                    <div class="card-body">

                        <div>
                            {{ Form::open(['route' => ['articles.update', $article->slug], "method" => "PATCH", 'id' => 'article-form',  "enctype" => "multipart/form-data"]) }}
                            <div>
                                <span>Title*</span>
                                <br>
                                {{ Form::text('title', $article->title, [ 'class' => '', 'id' => 'article-name' ]) }}
                            </div>
                            <div>
                                <span>Body*</span>
                                <br>
                                {{ Form::textarea('body', $article->body, [ 'class' => '', 'id' => 'article-body' ]) }}
                            </div>
                            <div>
                                {{ Form::button("Save", ["class" => "btn btn-primary", "type" => "submit"]) }}
                            </div>

                            {{ Form::close() }}
                        </div>

                        <br>
                        {{ Form::open(['route' => ['articles.destroy', $article->slug], "method" => "DELETE"]) }}
                        <div>
                            {{ Form::button("Delete", ["class" => "btn btn-danger", "type" => "submit"]) }}
                        </div>
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
