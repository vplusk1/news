@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Articles</div>

                    <div class="card-body">

                            <div>
                                <h3>{!! $article->title !!}</h3>
                                <p>
                                    {!! $article->body !!}
                                </p>
                                <p>Author: {!! $article->user->name !!}</p>
                                <p>{!! $article->created_at !!}</p>
                            </div>
                        @include('includes.comments', ['comments' => $comments])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
