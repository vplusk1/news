@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Articles</div>

                    <div class="card-body">
                        @foreach($articles as $article)
                            <div>
                                <a href="/articles/{!! $article->slug !!}">{!! $article->title !!}</a>
                                <p>
                                    {!! str_limit($article->body, 200) !!}
                                </p>

                                <p>Author: {!! $article->user->name !!}</p>
                                <p>{!! date('d-m-Y', strtotime($article->created_at)) !!}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
